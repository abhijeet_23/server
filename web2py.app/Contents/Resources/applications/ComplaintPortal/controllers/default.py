# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
#########################################################################

def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    response.flash = T("Hello World")
    return dict(message=T('Welcome to web2py!'))


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()

@request.restful()
def api():
    response.view = 'generic.'+request.extension
    def GET(*args,**vars):
        patterns = 'auto'
        parser = db.parse_as_rest(patterns,args,vars)
        if parser.status == 200:
            return dict(content=parser.response)
        else:
            raise HTTP(parser.status,parser.error)
    def POST(table_name,**vars):
        return db[table_name].validate_and_insert(**vars)
    def PUT(table_name,record_id,**vars):
        return db(db[table_name]._id==record_id).update(**vars)
    def DELETE(table_name,record_id):
        return db(db[table_name]._id==record_id).delete()
    return dict(GET=GET, POST=POST, PUT=PUT, DELETE=DELETE)

def appRegister():
    type = request.vars.type
    first_name = request.vars.first_name
    last_name = request.vars.last_name
    contact_no = request.vars.contact_no
    email = request.vars.email
    password = request.vars.password
    department = request.vars.department
    if type == '0':
        entry_no = request.vars.entry_no
        hostel = request.vars.hostel
        room_no = request.vars.hostel
        register = db.auth_user.validate_and_insert(first_name=first_name, last_name=last_name, contact_no = contact_no, email=email, password=password, department=department, entry_no = entry_no, hostel=hostel, room_no=room_no)
    else:
        entry_no = request.vars.employee_id
        office_address = request.vars.office_address
        office_number = request.vars.office_number
        register = db.auth_user.validate_and_insert(first_name=first_name, last_name=last_name, contact_no = contact_no, email=email, password=password, department=department, entry_no = entry_no, office_address=office_address, office_number=office_number)

    return dict(success = True if not register.errors else False, errors = register.errors)

def appLogin():
    password = request.vars.password
    email = request.vars.email
    login = auth.login_bare(email,password)
    return dict(success=False if not login else True, user=login)

def myComplaints():
    user_id = request.vars.user_id
    active = db((db.complaints.created_by == user_id) & (db.complaints.status == 0)).select()
    resolved = db((db.complaints.created_by == user_id) & (db.complaints.status == 1)).select()
    return dict(active = active, resolved = resolved)


def insertComplaint():
    complaint_type = request.vars.complaint_type
    category = request.vars.category
    sub_category = request.vars.sub_category
    hostel = request.vars.hostel
    complaint_location = request.vars.complaint_location
    description = request.vars.description
    picture = request.vars.picture
    user_id = request.vars.user_id
    insert = db.complaints.insert(complaint_type=complaint_type, category=category, sub_category = sub_category, hostel=hostel, complaint_location=complaint_location, description=description, status = 0, upvotes=0, upvote_ids="{}",picture=picture,  created_by = user_id)
    return dict(success = True if not insert.errors else False, errors = insert.errors)


def resolveComplaint():
    complaint_id = request.vars.complaint_id
    user_id = request.vars.user_id
    complaint_row = db((db.complaints.id == complaint_id)).select()[0]
    user_row = db((db.auth_user.id == user_id)).select()[0]
    institute_count = db((db.auth_membership.group_id == 3) & (db.auth_membership.user_id == user_id)).count()
    if((complaint_row.complaint_type == "Individual") & (complaint_row.created_by == int(user_id))):
        permit = True
    elif((complaint_row.complaint_type == "Institute") & (institute_count > 0)):
        permit = True
    elif((complaint_row.complaint_type == "Hostel") & (user_row.hostel == complaint_row.hostel)):
        hostel_count = db((db.auth_membership.group_id == 9) & (db.auth_membership.user_id == user_id)).count()
        if (hostel_count > 0):
            permit = True
        if (complaint_row.category == "Maintenance"):
            group_id = 8
        elif (complaint_row.category == "Mess"):
            group_id = 4
        hostel_count = db((db.auth_membership.group_id == group_id) & (db.auth_membership.user_id == user_id)).count()
        if (hostel_count > 0):
            permit = True
        else:
            permit = False
    else:
        permit = False
    if permit:
       return db((db.complaints.id == complaint_id)).update(status=1)
    else:
        return False
    
def upvote():
    import json
    complaint_id = request.vars.complaint_id
    user_id = request.vars.user_id
    complaint_row = db((db.complaints.id == complaint_id)).select()[0]
    jsonstring = complaint_row.upvote_ids
    exists = False
    upvote_ids = json.loads(jsonstring)
    for upvote_id in upvote_ids:
        if (upvote_ids[upvote_id] == int(user_id)):
            exists = True
            break
    if (not(exists)):
        upvotes = int(complaint_row.upvotes) + 1
        upvote_ids[upvotes] = int(user_id)
        if (db((db.complaints.id == complaint_id)).update(upvote_ids=json.dumps(upvote_ids), upvotes = upvotes)):
            return upvotes
        else:
            return complaint_row.upvotes
    else:
        return complaint_row.upvotes
