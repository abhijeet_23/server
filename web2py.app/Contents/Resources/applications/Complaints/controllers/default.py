# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
#########################################################################

def logged_in():
    return dict(success=auth.is_logged_in(), user=auth.user)

def logout():
    return dict(success=True, loggedout=auth.logout())

auth.settings.allow_basic_login = True
@auth.requires_login()
@request.restful()
def api():
    response.view = 'generic.'+request.extension
    def GET(*args,**vars):
        patterns = 'auto'
        parser = db.parse_as_rest(patterns,args,vars)
        if parser.status == 200:
            return dict(content=parser.response)
        else:
            raise HTTP(parser.status,parser.error)
    def POST(table_name,**vars):
        return db[table_name].validate_and_insert(**vars)
    def PUT(table_name,record_id,**vars):
        return db(db[table_name]._id==record_id).update(**vars)
    def DELETE(table_name,record_id):
        return db(db[table_name]._id==record_id).delete()
    return dict(GET=GET, POST=POST, PUT=PUT, DELETE=DELETE)

def login():
    userid = request.vars.userid
    password = request.vars.password
    user = auth.login_bare(userid,password)
    return dict(success=False if not user else True, user=user)

def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    response.flash = T("Hello World")
    return dict(message=T('Welcome to the IIT Delhi Complaints Management System!'))

def report():
    record = db.complaints(request.args(0)) #or redirect(URL('index'))
    db.complaints.upvotes.default=0
    db.complaints.downvotes.default=0
    db.complaints.upvotes.writable=db.complaints.upvotes.readable=False
    db.complaints.downvotes.writable=db.complaints.downvotes.readable=False
    form = SQLFORM(db.complaints, record)
    if form.process().accepted:
        response.flash = 'form accepted'
    elif form.errors:
        response.flash = 'form has errors'
    return dict(form=form)

def mycomplaints():
    rows = db(db.complaints.created_by == auth.user_id).select()
    # Display complaints if user_id is same as created_by's user id
    # Give rights to edit resolution status of individual complaints
    # Give option to add comments with "Commented on time by xyz"

    return locals()

def nearby():
    # Display complaints if hostel is same, and all institute complaints
    # Enable adding comment
    # Enable upvote & downvote
    # Give option to add comments with "Commented on time by xyz"

    return 0

def LostAndFound():
    # Display entire table
    # Claim as yours
    # Add a lost a item
    return 0

def admin():
    # Check if user has any admin rights
    #Give option to request for admin rights
    # Give rights to edit resolution status of institute level complaints & hostel level complaints
    # Add comment on post with "Comment by Moderator" written
    return 0

def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()
