# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## Customize your APP title, subtitle and menus here
#########################################################################

response.logo = A(B('IIT Delhi Complaints Portal'),XML('&nbsp;'),
                  _class="navbar-brand",_href="http://www.iitd.ac.in/",
                  _id="web2py-logo")
response.title = request.application.replace('_',' ').title()
response.subtitle = ''

## read more at http://dev.w3.org/html5/markup/meta.name.html
response.meta.author = 'Siddarth Jain <ee1120482@iitd.ac.in>'
response.meta.description = 'A online complaint portal for IIT Delhi community'
response.meta.keywords = 'web2py, python'
response.meta.generator = 'Web portal'

## your http://google.com/analytics id
response.google_analytics_id = None

#########################################################################
## this is the main application menu add/remove items as required
#########################################################################

response.menu = [
    (T('Home'), False, URL('default', 'index'), [])
]

DEVELOPMENT_MENU = True

#########################################################################
## provide shortcuts for development. remove in production
#########################################################################

def _():
    # shortcuts
    app = request.application
    ctr = request.controller
    # useful links to internal and external resources
    response.menu += [
        (T('Report a complaint'), False, URL('Complaints', 'default', 'report')),
          (T('My Complaints'), False,URL('Complaints', 'default', 'mycomplaints') ),
          ('Complaints Nearby', False,URL('Complaints', 'default', 'nearby')),
          (T('Lost and Found'), False, URL('Complaints', 'default', 'LostAndFound')),
          (T('Admin Roles'), False, URL('Complaints', 'default', 'admin')),
        ]
if DEVELOPMENT_MENU: _()

if "auth" in locals(): auth.wikimenu()
